package per.aferencz;

import per.aferencz.verification.AtLeastVerification;
import per.aferencz.verification.NeverVerification;
import per.aferencz.verification.NoInteractionVerifier;
import per.aferencz.verification.TimesVerification;
import per.aferencz.verification.VerificationType;

public final class Verifiers {
    public static VerificationType times(final int times) {
        return times > 0 ? new TimesVerification(times) : new NeverVerification();
    }

    public static VerificationType atLeast(final int times) {
        return times > 0 ? new AtLeastVerification(times) : new NeverVerification();
    }

    public static VerificationType never() {
        return new NeverVerification();
    }

    public static VerificationType noInteractionAtAll() {
        return new NoInteractionVerifier();
    }
}
