package per.aferencz.matcher;

public class ContainsMatcher implements Matcher {
    private final CharSequence shouldContain;

    public ContainsMatcher(final CharSequence shouldContain) {
        this.shouldContain = shouldContain;
    }

    @Override
    public boolean verify(final Object object) {
        assert object instanceof String;

        return ((String) object).contains(shouldContain);
    }
}
