package per.aferencz.matcher;

import java.util.Objects;

public class StaticValueMatcher implements Matcher {
    private final Object expectedValue;

    public StaticValueMatcher(Object expectedValue) {
        this.expectedValue = expectedValue;
    }

    @Override
    public boolean verify(final Object object) {
        return Objects.equals(expectedValue, object);
    }
}
