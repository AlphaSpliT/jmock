package per.aferencz.matcher;

public class AnyMatcher implements Matcher {
    @Override
    public boolean verify(final Object object) {
        return object != null;
    }
}
