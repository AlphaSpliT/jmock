package per.aferencz.matcher;

public class AnyTypeMatcher implements Matcher {
    private final Class<?> typeToMatch;

    public AnyTypeMatcher(final Class<?> typeToMatch) {
        this.typeToMatch = typeToMatch;
    }

    @Override
    public boolean verify(final Object object) {
        return object != null && typeToMatch.isAssignableFrom(object.getClass());
    }
}
