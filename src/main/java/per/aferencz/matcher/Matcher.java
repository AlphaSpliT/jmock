package per.aferencz.matcher;

import java.io.Serializable;

public interface Matcher extends Serializable {
    boolean verify(final Object object);
}
