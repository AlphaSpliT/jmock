package per.aferencz.matcher;

import java.util.List;

public class AnyMatcherArgumentCaptor<T> implements Matcher {
    private final List<T> argumentCallList;

    public AnyMatcherArgumentCaptor(final List<T> argumentCallList) {
        this.argumentCallList = argumentCallList;
    }

    @Override
    public boolean verify(final Object object) {
        argumentCallList.add(object == null ? null : (T) object);
        return object != null;
    }
}
