package per.aferencz.mapper;

import java.util.HashMap;
import java.util.Objects;

public final class OriginalClassMapper extends HashMap<Object, Class<?>> {
    private static OriginalClassMapper INSTANCE;

    private OriginalClassMapper() {
    }

    public static OriginalClassMapper getInstance() {
        synchronized (MatcherStack.class) {
            if (Objects.isNull(INSTANCE)) {
                INSTANCE = new OriginalClassMapper();
            }
        }

        return INSTANCE;
    }
}
