package per.aferencz.mapper;

import java.util.Objects;
import java.util.Stack;
import per.aferencz.matcher.Matcher;

public final class MatcherStack extends Stack<Matcher> {
    private static MatcherStack INSTANCE;

    private MatcherStack() {
    }

    public static MatcherStack getInstance() {
        synchronized (MatcherStack.class) {
            if (Objects.isNull(INSTANCE)) {
                INSTANCE = new MatcherStack();
            }
        }

        return INSTANCE;
    }
}
