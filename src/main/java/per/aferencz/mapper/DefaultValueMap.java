package per.aferencz.mapper;

import org.apache.commons.lang3.ClassUtils;

import java.util.*;
import java.util.stream.Stream;

public final class DefaultValueMap extends HashMap<Class<?>, Object> {
    private static DefaultValueMap INSTANCE;

    private DefaultValueMap() {
        put(int.class, 0);
        put(double.class, 0.0);
        put(float.class, 0.0f);
        put(long.class, 0L);
        put(boolean.class, false);
        put(short.class, 0);
        put(byte.class, 0);
        put(char.class, '\0');
        put(Integer.class, 0);
        put(Double.class, 0.0);
        put(Float.class, 0.0f);
        put(Long.class, 0L);
        put(Boolean.class, false);
        put(Short.class, 0);
        put(Byte.class, 0);
        put(Character.class, '\0');
        put(String.class, "");
        put(List.class, Collections.emptyList());
        put(Set.class, Collections.emptySet());
        put(Collection.class, Collections.emptyList());
        put(Stream.class, Stream.empty());
        put(Map.class, Map.of());
    }

    public static DefaultValueMap getInstance() {
        synchronized (DefaultValueMap.class) {
            if (Objects.isNull(INSTANCE)) {
                INSTANCE = new DefaultValueMap();
            }
        }

        return INSTANCE;
    }

    @SuppressWarnings("unchecked")
    public <T> T forClass(final Class<T> tClass) {
        T result = (T) getOrDefault(tClass, null);

        if (result == null) {
            Optional<Class<?>> assignable = keySet().stream().filter(savedClass -> ClassUtils.isAssignable(tClass, savedClass)).findAny();

            if (assignable.isPresent()) {
                result = (T) get(assignable.get());
            }
        }

        return result;
    }
}
