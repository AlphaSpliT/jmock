package per.aferencz.mapper;

import java.util.Objects;
import java.util.Stack;
import per.aferencz.data.MethodInvocation;

public final class MockInvocationStack extends Stack<MethodInvocation> {
    private static MockInvocationStack INSTANCE;

    private MockInvocationStack() {
    }

    public static MockInvocationStack getInstance() {
        synchronized (MatcherStack.class) {
            if (Objects.isNull(INSTANCE)) {
                INSTANCE = new MockInvocationStack();
            }
        }

        return INSTANCE;
    }
}
