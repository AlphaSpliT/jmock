package per.aferencz.mapper;

import java.util.HashMap;
import java.util.Objects;

public final class VerificationMockMapper extends HashMap<Object, Object> {
    private static VerificationMockMapper INSTANCE;

    private VerificationMockMapper() {
    }

    public static VerificationMockMapper getInstance() {
        synchronized (MatcherStack.class) {
            if (Objects.isNull(INSTANCE)) {
                INSTANCE = new VerificationMockMapper();
            }
        }

        return INSTANCE;
    }
}
