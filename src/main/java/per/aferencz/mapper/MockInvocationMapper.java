package per.aferencz.mapper;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import per.aferencz.invocation.TestInvocationHandler;

public final class MockInvocationMapper {
    private static MockInvocationMapper INSTANCE;
    private final Map<Object, TestInvocationHandler> mockToTestInvocationHandlerMap;

    private MockInvocationMapper() {
        this.mockToTestInvocationHandlerMap = new HashMap<>();
    }

    public static MockInvocationMapper getInstance() {
        synchronized (MockInvocationMapper.class) {
            if (Objects.isNull(INSTANCE)) {
                INSTANCE = new MockInvocationMapper();
            }
        }

        return INSTANCE;
    }

    public TestInvocationHandler get(final Object mock) {
        return mockToTestInvocationHandlerMap.getOrDefault(mock, null);
    }

    public void map(final Object mock, final TestInvocationHandler invocationHandler) {
        mockToTestInvocationHandlerMap.put(mock, invocationHandler);
    }
}