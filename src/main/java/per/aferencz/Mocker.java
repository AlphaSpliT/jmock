package per.aferencz;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.description.ByteCodeElement;
import net.bytebuddy.implementation.InvocationHandlerAdapter;
import net.bytebuddy.matcher.ElementMatcher;
import per.aferencz.data.MethodInvocation;
import per.aferencz.data.ReturnModifier;
import per.aferencz.invocation.TestInvocationHandler;
import per.aferencz.invocation.VerificationInvocationHandler;
import per.aferencz.mapper.DefaultValueMap;
import per.aferencz.mapper.MockInvocationMapper;
import per.aferencz.mapper.MockInvocationStack;
import per.aferencz.mapper.OriginalClassMapper;
import per.aferencz.mapper.VerificationMockMapper;
import per.aferencz.returner.StaticValueReturner;
import per.aferencz.verification.VerificationData;
import per.aferencz.verification.VerificationLevel;
import per.aferencz.verification.VerificationType;

import static net.bytebuddy.matcher.ElementMatchers.isDeclaredBy;
import static per.aferencz.Verifiers.times;

public final class Mocker {
    private static final Set<Class<?>> MOCKED_TYPES = new HashSet<>();
    private static final DefaultValueMap DEFAULT_VALUE_MAP = DefaultValueMap.getInstance();

    public static List<Class<?>> getSuperClasses(final Class<?> aClass) {
        List<Class<?>> classes = new ArrayList<>();

        Class<?> root = aClass;

        while (root != null) {
            classes.add(root);
            root = root.getSuperclass();
        }

        return classes;
    }

    public static Set<Class<?>> getSuperInterfaces(final Class<?> aClass) {
        Set<Class<?>> classes = new HashSet<>();

        classes.add(aClass);

        for (Class<?> anInterface : aClass.getInterfaces()) {
            classes.add(anInterface);
            classes.addAll(getSuperInterfaces(anInterface));
        }

        return classes;
    }

    public static <T> T mock(final Class<T> aClass) {
        TestInvocationHandler invocationHandler = new TestInvocationHandler();

        Collection<Class<?>> classesToTraverse = aClass.isInterface() ? getSuperInterfaces(aClass) : getSuperClasses(aClass);

        ElementMatcher.Junction<ByteCodeElement> declaredBy = isDeclaredBy(aClass);

        if (aClass.isInterface()) {
            classesToTraverse.add(Object.class);
        } else {
            classesToTraverse.remove(Object.class);
        }

        classesToTraverse.remove(aClass);

        for (Class<?> superClass : classesToTraverse) {
            declaredBy = declaredBy.or(isDeclaredBy(superClass));
        }

        classesToTraverse.add(aClass);

        classesToTraverse.forEach(
                superClass -> {
                    for (Method declaredMethod : superClass.getDeclaredMethods()) {
                        invocationHandler.addRunner(declaredMethod, new StaticValueReturner(DEFAULT_VALUE_MAP.forClass(declaredMethod.getReturnType())));
                    }
                }
        );

        Class<? extends T> mockType = new ByteBuddy()
                .subclass(aClass)
                .method(declaredBy)
                .intercept(InvocationHandlerAdapter.of(invocationHandler))
                .name("Mock".concat(aClass.getSimpleName()))
                .make()
                .load(ClassLoader.getSystemClassLoader())
                .getLoaded();

        T result = newInstance(mockType);

        MOCKED_TYPES.add(mockType);

        MockInvocationMapper.getInstance().map(result, invocationHandler);
        OriginalClassMapper.getInstance().put(result, aClass);

        return result;
    }

    public static <T> T verify(final T object) {
        return verify(object, times(1));
    }

    @SuppressWarnings("unchecked")
    public static <T> T verify(final T object, final VerificationType verificationType) {
        InvocationHandler verificationInvocationHandler = new VerificationInvocationHandler(verificationType);

        if (verificationType.getLevel().equals(VerificationLevel.OBJECT)) {
            verifyInPlace(object, verificationType);
        }

        if (!MOCKED_TYPES.contains(object.getClass())) {
            throw new RuntimeException(object.getClass().getCanonicalName() + " is probably not a mocked object!");
        }

        Class<?> originalClass = OriginalClassMapper.getInstance().get(object);

        Set<Class<?>> classesToTraverse = new HashSet<>(originalClass.isInterface() ? getSuperInterfaces(originalClass) : getSuperClasses(originalClass));

        classesToTraverse.remove(Object.class);
        classesToTraverse.remove(originalClass);

        ElementMatcher.Junction<ByteCodeElement> declaredBy = isDeclaredBy(originalClass);

        for (Class<?> superClass : classesToTraverse) {
            declaredBy = declaredBy.or(isDeclaredBy(superClass));
        }

        Class<? extends T> mockType = (Class<? extends T>) new ByteBuddy()
                .subclass(originalClass)
                .method(declaredBy)
                .intercept(InvocationHandlerAdapter.of(verificationInvocationHandler))
                .name("Verify".concat(originalClass.getSimpleName()))
                .make()
                .load(ClassLoader.getSystemClassLoader())
                .getLoaded();

        T result = newInstance(mockType);

        if (Objects.nonNull(result)) {
            VerificationMockMapper.getInstance().put(result, object);
        }

        return result;
    }

    private static <T> void verifyInPlace(final T object, final VerificationType verificationType) {
        TestInvocationHandler testInvocationHandler = MockInvocationMapper.getInstance().get(object);

        VerificationData verificationData = new VerificationData(
                Collections.emptyList(),
                testInvocationHandler.getAllMethodCalls(),
                object.getClass());

        if (!verificationType.verify(verificationData)) {
            throw new AssertionError("VerificationError:\n" + verificationType.toString());
        }
    }

    @SuppressWarnings("unchecked")
    private static <T> T newInstance(Class<? extends T> mockType) {
        Optional<Constructor<?>> first = Arrays.stream(mockType.getConstructors()).findFirst();

        if (first.isEmpty()) {
            throw new RuntimeException("Cannot find constructor to construct object");
        }

        Constructor<?> mockTypeConstructor = first.get();

        T result = null;
        try {
            result = (T) mockTypeConstructor.newInstance(Arrays.stream(mockTypeConstructor.getParameterTypes()).map(
                    param -> DEFAULT_VALUE_MAP.getOrDefault(param, null)
            ).toArray());
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static <T> T redefine(final Class<T> classToRedefine, Object ... arguments) {
        Class<? extends T> newType = new ByteBuddy()
                .subclass(classToRedefine)
                .name(classToRedefine.getSimpleName().concat("Redefined"))
                .make()
                .load(ClassLoader.getSystemClassLoader())
                .getLoaded();
        return newInstance(newType);
    }

    @SuppressWarnings("unchecked")
    public static <T> ReturnModifier<T> when(final T val) {
        MethodInvocation methodInvocation = MockInvocationStack.getInstance().pop();
        TestInvocationHandler testInvocationHandler = MockInvocationMapper.getInstance().get(methodInvocation.getInvokedObject());
        testInvocationHandler.popMethodCall(methodInvocation.getMethod());
        return new ReturnModifier<>((Class<T>) methodInvocation.getMethod().getReturnType(), testInvocationHandler, methodInvocation);
    }
}
