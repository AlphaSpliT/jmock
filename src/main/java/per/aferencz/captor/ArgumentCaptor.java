package per.aferencz.captor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import per.aferencz.mapper.MatcherStack;
import per.aferencz.matcher.AnyMatcherArgumentCaptor;

public final class ArgumentCaptor<T> {
    private final List<T> arguments;

    public ArgumentCaptor() {
        this.arguments =  new ArrayList<>();
    }

    public T capture() {
        MatcherStack.getInstance().push(new AnyMatcherArgumentCaptor<T>(arguments));
        return null;
    }

    public List<T> getCaptures() {
        return arguments.stream().filter(Objects::nonNull).collect(Collectors.toList());
    }
}
