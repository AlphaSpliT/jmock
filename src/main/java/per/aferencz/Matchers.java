package per.aferencz;

import per.aferencz.mapper.MatcherStack;
import per.aferencz.matcher.AnyMatcher;
import per.aferencz.matcher.AnyTypeMatcher;
import per.aferencz.matcher.ContainsMatcher;
import per.aferencz.matcher.StaticValueMatcher;

public final class Matchers {
    public static <T> T any() {
        MatcherStack.getInstance().push(new AnyMatcher());

        return null;
    }

    public static <T> T any(final Class<T> classToMatch) {
        MatcherStack.getInstance().push(new AnyTypeMatcher(classToMatch));

        return null;
    }

    public static <T> T eq(final T val) {
        MatcherStack.getInstance().push(new StaticValueMatcher(val));
        return val;
    }

    public static <T extends CharSequence> T contains(final T toMatch) {
        MatcherStack.getInstance().push(new ContainsMatcher(toMatch));
        return toMatch;
    }
}
