package per.aferencz.invocation;


import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import per.aferencz.data.MethodCall;
import per.aferencz.data.MethodInvocation;
import per.aferencz.mapper.MockInvocationStack;
import per.aferencz.returner.Returner;

public class TestInvocationHandler implements InvocationHandler {
    private final Map<Method, Integer> callMap;
    private final Map<Method, List<MethodCall>> parameterCallMap;
    private final Map<Method, List<Returner>> returnTypeMap;

    public TestInvocationHandler() {
        this.callMap = new HashMap<>();
        this.parameterCallMap = new HashMap<>();
        this.returnTypeMap = new HashMap<>();
    }

    public List<MethodCall> getMethodCalls(final Method method) {
        Optional<Method> methodOptional = this.parameterCallMap.keySet().stream().filter(mockMethod ->
                                                                                                 mockMethod.getName().equals(method.getName()) && Arrays.equals(
                                                                                                         mockMethod.getParameterTypes(),
                                                                                                         method.getParameterTypes())
        ).findFirst();

        List<MethodCall> result = Collections.emptyList();

        if (methodOptional.isPresent()) {
            result = this.parameterCallMap.get(methodOptional.get());
        }

        return result;
    }

    public Map<Method, List<MethodCall>> getAllMethodCalls() {
        return new HashMap<>(parameterCallMap);
    }

    public MethodCall popMethodCall(final Method method) {
        MethodCall remove = null;
        if (parameterCallMap.containsKey(method)) {
            List<MethodCall> methodCalls = parameterCallMap.get(method);
            remove = methodCalls.remove(methodCalls.size() - 1);
        }

        return remove;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) {
        if (!callMap.containsKey(method)) {
            callMap.put(method, 1);
        } else {
            callMap.put(method, callMap.get(method) + 1);
        }

        if (!parameterCallMap.containsKey(method)) {
            parameterCallMap.put(method, new ArrayList<>(Collections.singletonList(new MethodCall(method, Arrays.asList(args)))));
        } else {
            parameterCallMap.get(method).add(new MethodCall(method, Arrays.asList(args)));
        }

        MethodInvocation methodInvocation = new MethodInvocation(
                proxy,
                new ArrayList<>(Arrays.asList(args)),
                method
        );

        MockInvocationStack.getInstance().push(methodInvocation);

        return returnTypeMap.getOrDefault(method, Collections.emptyList())
                            .stream()
                            .map(returner -> returner.get(args))
                            .filter(Objects::nonNull)
                            .findFirst()
                            .orElse(null);
    }

    public void addRunner(Method declaredMethod, Returner returner) {
        if (this.returnTypeMap.containsKey(declaredMethod)) {
            this.returnTypeMap.get(declaredMethod).add(0, returner);
        } else {
            this.returnTypeMap.put(declaredMethod, new ArrayList<>(Collections.singletonList(returner)));
        }
    }
}
