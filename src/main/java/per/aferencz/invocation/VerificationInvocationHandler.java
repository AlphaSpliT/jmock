package per.aferencz.invocation;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import per.aferencz.data.MethodCall;
import per.aferencz.mapper.DefaultValueMap;
import per.aferencz.mapper.MatcherStack;
import per.aferencz.mapper.MockInvocationMapper;
import per.aferencz.mapper.VerificationMockMapper;
import per.aferencz.matcher.Matcher;
import per.aferencz.matcher.StaticValueMatcher;
import per.aferencz.verification.NeverVerification;
import per.aferencz.verification.VerificationData;
import per.aferencz.verification.VerificationType;

public class VerificationInvocationHandler implements InvocationHandler {
    private final VerificationType verificationType;

    public VerificationInvocationHandler(final VerificationType verificationType) {
        this.verificationType = verificationType;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) {
        Object originalMock = VerificationMockMapper.getInstance().getOrDefault(proxy, null);

        if (Objects.isNull(originalMock)) {
            throw new RuntimeException("You are trying to verify a non mocked object!");
        }

        TestInvocationHandler testInvocationHandler = MockInvocationMapper.getInstance().get(originalMock);

        boolean verificationResult = verify(testInvocationHandler, method, originalMock.getClass());

        List<MethodCall> methodCalls = testInvocationHandler.getMethodCalls(method);

        ArrayList<Matcher> parameterMatchers = new ArrayList<>();

        if (MatcherStack.getInstance().size() < args.length && !MatcherStack.getInstance().empty()) {
            throw new RuntimeException("You seem to be interchanging per.aferencz.Matchers and static values. This is not supported. Please use either");
        }

        for (int i = args.length - 1; i >= 0; --i) {
            Object parameter = args[i];

            if (!MatcherStack.getInstance().empty()) {
                parameterMatchers.add(MatcherStack.getInstance().pop());
            } else {
                parameterMatchers.add(new StaticValueMatcher(parameter));
            }
        }

        Collections.reverse(parameterMatchers);

        boolean called = false;

        for (MethodCall methodCall : methodCalls) {
            boolean matches = true;
            for (int i = 0; i < parameterMatchers.size(); ++i) {
                Matcher matcher = parameterMatchers.get(i);
                Object usedParameter = methodCall.getParameterList().get(i);
                if (!matcher.verify(usedParameter)) {
                    matches = false;
                }
            }

            if (matches) {
                called = true;
            }
        }

        if (!called && !verificationException(verificationResult)) {
            StringBuilder messageBuilder = new StringBuilder()
                    .append("Method ")
                    .append(method.getName())
                    .append(" was called ")
                    .append(methodCalls.size())
                    .append(" times, but never with arguments (");

            for (Object arg : args) {
                messageBuilder.append(arg)
                              .append(", ");
            }

            messageBuilder.append(")");

            throw new AssertionError(messageBuilder.toString());
        }

        return DefaultValueMap.getInstance().forClass(method.getReturnType());
    }

    private boolean verificationException(final boolean verificationResult) {
        return verificationType.getClass().equals(NeverVerification.class) && verificationResult;
    }

    private boolean verify(final TestInvocationHandler testInvocationHandler, final Method method, final Class<?> mockType) {
        VerificationData verificationData = new VerificationData(
                testInvocationHandler.getMethodCalls(method),
                testInvocationHandler.getAllMethodCalls(),
                mockType);

        if (!verificationType.verify(verificationData)) {
            throw new AssertionError("VerificationError:\n" + verificationType.toString());
        }

        return true;
    }
}
