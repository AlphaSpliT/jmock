package per.aferencz.returner;


import java.util.List;
import java.util.stream.IntStream;
import per.aferencz.matcher.Matcher;

public class ArgumentMatcherStaticValueReturner implements Returner {
    private final List<Matcher> matcherList;
    private final Object objectToReturn;

    public ArgumentMatcherStaticValueReturner(final List<Matcher> matcherList, final Object objectToReturn) {
        this.matcherList = List.copyOf(matcherList);
        this.objectToReturn = objectToReturn;
    }

    @Override
    public Object get(Object[] args) {
        boolean argumentsMatch = IntStream.range(0, matcherList.size())
                                          .map(i -> matcherList.get(i).verify(args[i]) ? 1 : 0)
                                          .allMatch(result -> result == 1);

        return argumentsMatch ? objectToReturn : null;
    }
}
