package per.aferencz.returner;

public interface Returner {
    Object get(Object[] args);
}
