package per.aferencz.returner;

public class StaticValueReturner implements Returner {
    private final Object valueToReturn;

    public StaticValueReturner(final Object valueToReturn) {
        this.valueToReturn = valueToReturn;
    }

    @Override
    public Object get(Object[] args) {
        return valueToReturn;
    }
}
