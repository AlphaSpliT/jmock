package per.aferencz.verification;

public interface VerificationType {
    boolean verify(final VerificationData verificationData);

    VerificationLevel getLevel();
}
