package per.aferencz.verification;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.lang3.builder.ToStringBuilder;
import per.aferencz.Mocker;

public class NoInteractionVerifier implements VerificationType {
    private Set<String> butInteractedWith;

    @Override
    public boolean verify(final VerificationData verificationData) {
        Set<Class<?>> superClasses = new HashSet<>(Mocker.getSuperClasses(verificationData.getMockType()));
        superClasses.remove(Object.class);
        Set<Method> methodsToCheck = superClasses
                .stream()
                .flatMap(aClass -> Arrays.stream(aClass.getDeclaredMethods())).collect(Collectors.toSet());

        butInteractedWith = verificationData.getMethodCallsMap()
                                            .keySet()
                                            .stream()
                                            .filter(methodsToCheck::contains)
                                            .map(Method::getName)
                                            .collect(Collectors.toSet());

        return methodsToCheck.stream().noneMatch(verificationData.getMethodCallsMap()::containsKey);
    }

    @Override
    public VerificationLevel getLevel() {
        return VerificationLevel.OBJECT;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("butInteractedWith", butInteractedWith)
                .toString();
    }
}
