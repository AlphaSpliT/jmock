package per.aferencz.verification;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import per.aferencz.data.MethodCall;

public class VerificationData {
    private final List<MethodCall> methodCallsList;
    private final Map<Method, List<MethodCall>> methodCallsMap;
    private final Class<?> mockType;

    public VerificationData(
            final List<MethodCall> methodCallsList,
            final Map<Method, List<MethodCall>> methodCallsMap, final Class<?> mockType) {
        this.methodCallsList = methodCallsList;
        this.methodCallsMap = methodCallsMap;
        this.mockType = mockType;
    }

    public List<MethodCall> getMethodCallsList() {
        return List.copyOf(methodCallsList);
    }

    public Map<Method, List<MethodCall>> getMethodCallsMap() {
        return new HashMap<>(methodCallsMap);
    }

    public Class<?> getMockType() {
        return mockType;
    }
}
