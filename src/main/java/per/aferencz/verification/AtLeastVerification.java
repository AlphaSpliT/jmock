package per.aferencz.verification;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class AtLeastVerification implements VerificationType {
    private final int atLeast;
    private int butCalled;

    public AtLeastVerification(final int atLeast) {
        this.atLeast = atLeast;
    }

    @Override
    public boolean verify(final VerificationData verificationData) {
        butCalled = verificationData.getMethodCallsList().size();
        return verificationData.getMethodCallsList().size() >= atLeast;
    }

    @Override
    public VerificationLevel getLevel() {
        return VerificationLevel.METHOD;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("atLeast", atLeast)
                .append("butCalled", butCalled)
                .toString();
    }
}
