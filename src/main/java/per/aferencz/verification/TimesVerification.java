package per.aferencz.verification;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class TimesVerification implements VerificationType {
    private final int shouldBeCalled;
    private int butCalled;

    public TimesVerification(final int times) {
        this.shouldBeCalled = times;
    }

    @Override
    public boolean verify(final VerificationData verificationData) {
        butCalled = verificationData.getMethodCallsList().size();
        return verificationData.getMethodCallsList().size() == shouldBeCalled;
    }

    @Override
    public VerificationLevel getLevel() {
        return VerificationLevel.METHOD;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("shouldBeCalled", shouldBeCalled)
                .append("butCalled", butCalled)
                .toString();
    }
}
