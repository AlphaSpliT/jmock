package per.aferencz.verification;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class NoVerification implements VerificationType {
    @Override
    public boolean verify(final VerificationData verificationData) {
        return true;
    }

    @Override
    public VerificationLevel getLevel() {
        return VerificationLevel.METHOD;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .toString();
    }
}
