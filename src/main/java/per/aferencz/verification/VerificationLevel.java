package per.aferencz.verification;

public enum VerificationLevel {
    METHOD,
    OBJECT
}
