package per.aferencz.verification;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class NeverVerification implements VerificationType {
    private int butCalled;

    @Override
    public boolean verify(final VerificationData verificationData) {
        butCalled = verificationData.getMethodCallsList().size();
        return verificationData.getMethodCallsList().size() == 0;
    }

    @Override
    public VerificationLevel getLevel() {
        return VerificationLevel.METHOD;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("butCalled", butCalled)
                .toString();
    }
}
