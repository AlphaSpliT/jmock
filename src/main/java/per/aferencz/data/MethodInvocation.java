package per.aferencz.data;

import java.lang.reflect.Method;
import java.util.List;

public class MethodInvocation {
    private final Object invokedObject;
    private final List<Object> arguments;
    private final Method method;

    public MethodInvocation(Object invokedObject, List<Object> arguments, Method method) {

        this.invokedObject = invokedObject;
        this.arguments = arguments;
        this.method = method;
    }

    public Object getInvokedObject() {
        return invokedObject;
    }

    public List<Object> getArguments() {
        return arguments;
    }

    public Method getMethod() {
        return method;
    }

    @Override
    public String toString() {
        return new org.apache.commons.lang3.builder.ToStringBuilder(this)
                .append("invokedObject", invokedObject)
                .append("arguments", arguments)
                .append("method", method)
                .toString();
    }
}
