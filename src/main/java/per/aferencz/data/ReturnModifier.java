package per.aferencz.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import per.aferencz.invocation.TestInvocationHandler;
import per.aferencz.mapper.MatcherStack;
import per.aferencz.matcher.Matcher;
import per.aferencz.matcher.StaticValueMatcher;
import per.aferencz.returner.ArgumentMatcherStaticValueReturner;
import per.aferencz.returner.StaticValueReturner;

public class ReturnModifier<T> {
    private final Class<T> returnType;
    private final TestInvocationHandler testInvocationHandler;
    private final MethodInvocation methodInvocation;

    public ReturnModifier(Class<T> returnType, final TestInvocationHandler testInvocationHandler, final MethodInvocation methodInvocation) {
        this.returnType = returnType;
        this.testInvocationHandler = testInvocationHandler;
        this.methodInvocation = methodInvocation;
    }

    public void thenReturn(final T value) {
        if (methodInvocation.getMethod().getParameterTypes().length == 0) {
            this.testInvocationHandler.addRunner(methodInvocation.getMethod(), new StaticValueReturner(value));
        } else {
            List<Matcher> matchers = new ArrayList<>();
            int numberOfParameters = methodInvocation.getMethod().getParameterTypes().length;

            if (MatcherStack.getInstance().size() < numberOfParameters && !MatcherStack.getInstance().empty()) {
                throw new RuntimeException("You seem to be interchanging per.aferencz.Matchers and static values. This is not supported. Please use either");
            }

            for (int i = numberOfParameters - 1; i >= 0; --i) {
                if (!MatcherStack.getInstance().isEmpty()) {
                    matchers.add(MatcherStack.getInstance().pop());
                } else {
                    matchers.add(new StaticValueMatcher(methodInvocation.getArguments().get(i)));
                }
            }
            Collections.reverse(matchers);

            this.testInvocationHandler.addRunner(
                    methodInvocation.getMethod(),
                    new ArgumentMatcherStaticValueReturner(matchers, value));
        }
    }
}
