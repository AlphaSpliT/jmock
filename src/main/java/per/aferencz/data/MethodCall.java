package per.aferencz.data;

import java.lang.reflect.Method;
import java.util.List;

public class MethodCall {
    private final Method method;
    private final List<Object> parameterList;

    public MethodCall(final Method method, final List<Object> parameterList) {
        this.method = method;
        this.parameterList = parameterList;
    }

    public Method getMethod() {
        return method;
    }

    public List<Object> getParameterList() {
        return List.copyOf(parameterList);
    }
}
